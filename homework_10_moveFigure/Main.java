package homework_10_moveFigure;

public class Main {
    public static void main(String[] args) {
        MoveFigure[] massiveFigure = new MoveFigure[3];

        Circle circle = new Circle(4, 5, 10, 20);
        massiveFigure[0] = circle;
        Square square = new Square(34, 56, -200 , -190);
        massiveFigure[1] = square;
        Square square1 = new Square(34, 56, -200 , -190);
        massiveFigure[2] = square1;

        for(int i = 0; i < massiveFigure.length; i++) {
            massiveFigure[i].moveFigureToNewCoordinate(0, 10, 20, 30);
        }
    }
}
