package homework_10_moveFigure;

public abstract class Figure {
    protected double x;
    protected double y;
    protected double x1;
    protected double y1;

    public Figure(double x, double y, double x1, double y1) {
        this.x = x;
        this.y = y;
        this.x1 = x1;
        this.y1 = y1;
    }
}
