package homework_07_min_number;

import java.util.Scanner;


public class Main {


    public static void main(String[] args) {
        System.out.println(findMinNumber());
    }


    public static int findMinNumber() {

        int number = 0;
        int[] tab = new int[201];
        int minNumber = 0;
        int minNumberI = 2147483647;
        Scanner in = new Scanner(System.in);

        while (number != -1) {
            number = in.nextInt();
            tab[number +100]++;
        }

        for (int i = 0; i < 200; i++)
            if (tab[i] != 0) {
                if (tab[i] < minNumberI) {
                   minNumber = i;
                }
            }
        return minNumber -100;
    }
}