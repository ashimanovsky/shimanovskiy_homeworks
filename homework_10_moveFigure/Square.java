package homework_10_moveFigure;

public class Square extends Rectangle implements MoveFigure {

    public Square(double x, double y, double x1, double y1) {
        super(x, y, x1, y1);
    }

    @Override
    public void moveFigureToNewCoordinate(double x, double y, double x1, double y1) {
        this.x = x;
        this.y = y;
        this.x1 = x1;
        this.y1 = y1;
        System.out.println("Квадрат перемещен");
    }
}
