package Homework_08;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int numberOfPeople = 10;
        Scanner console =new Scanner(System.in);
        Human[] humans = new Human[numberOfPeople];

        for(int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            System.out.println("Введите имя " + "человека " + (i+1));
            humans[i].setName(console.next());
            System.out.println("Введите массу " + "человека" + (i + 1));
            humans[i].setWeight(console.nextFloat());
        }


        for (int i = 0; i < humans.length; i++) {
            for (int j = i + 1; j < humans.length; j++) {
                if (humans[i].getWeight() > humans[j].getWeight()) {
                    Human temp = humans[i];
                    humans[i] = humans[j];
                    humans[j] = temp;
                }
            }
            System.out.print(humans[i].getName() + " ");
        }
    }
}
