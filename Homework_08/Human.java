package Homework_08;

public class Human {

    private String name;
    private double weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        if(weight < 0 || weight > 200) {
            weight = 0;
        }
        this.weight = weight;
    }
}